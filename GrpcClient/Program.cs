﻿using Grpc.Core;
using Grpc.Net.Client;
using GrpcServer;
using System;
using System.IO;
using System.Globalization;
using System.Threading.Tasks;


namespace GrpcClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");

            Console.WriteLine("Enter a date: ");
            string line = Console.ReadLine();
            DateTime dt;
            while (!DateTime.TryParseExact(line, "MM/dd/yyyy", null, System.Globalization.DateTimeStyles.None, out dt))
            {
                Console.WriteLine("Invalid date, please retry");
                line = Console.ReadLine();
            }
            Console.WriteLine("Valid date!");
            var input = new HelloRequest { Line = line };
            var client = new Greeter.GreeterClient(channel);
            var reply = await client.SayHelloAsync(input);
            Console.WriteLine(reply.Message);

            Console.WriteLine("Do you want to know your zodiac sign?\n 1. YES!\n 2. NO!");
            
            do
            {
                var key = Console.ReadKey();
                

                switch (key.Key)
                {
                    case ConsoleKey.D1:
                        {
                            GrpcClient.Zodiac zodSigns = new Zodiac();
                           
                            Console.WriteLine(". Enter again the day and the month of the date: ");
                            string month;
                            int day = Convert.ToInt32(Console.ReadLine());
                            month = Console.ReadLine();
                            Console.WriteLine(reply.Message);
                            Console.Write("The sign is: ");
                            Zodiac.zodiac_sign(day, month);
                            break;
                        } 
                    case ConsoleKey.D2:
                        {
                            Console.WriteLine(". Exit!");
                            break;
                        }
                } while (true) ;

            } while (true);
           
        }

    }
    }



